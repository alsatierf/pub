(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
	"straight/repos/straight.el/bootstrap.el"
	(or (bound-and-true-p straight-base-dir)
	    user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

;; Configure use-package to use straight.el by default
(use-package straight
  :custom
  (straight-use-package-by-default t))

(straight-use-package
 '(eat :type git
       :host codeberg
       :repo "akib/emacs-eat"
       :files ("*.el" ("term" "term/*.el") "*.texi"
	       "*.ti" ("terminfo/e" "terminfo/e/*")
	       ("terminfo/65" "terminfo/65/*")
	       ("integration" "integration/*")
	       (:exclude ".dir-locals.el" "*-tests.el"))))

(defmacro af/unread-key-sequence (sequence)
  `(lambda () (interactive) (setq-local unread-command-events (nconc (listify-key-sequence (kbd ,sequence)) unread-command-events))))

(use-package eat
  :ensure t
  :config
  (setq eat-term-name "xterm-256color")
  (customize-set-variable 'eat-semi-char-non-bound-keys '([?\C-x] [?\C-\\] [?\C-q] [?\C-g] [?\C-h] [?\e ?\C-c] [?\C-u] [?\e ?x] [?\e ?:] [?\e ?!] [?\e ?&] [?\e ?s] [?\e ?\;] [?\e ?h] [?\C-o]))
  (customize-set-variable 'eat-eshell-semi-char-non-bound-keys '([?\C-x] [?\C-\\] [?\C-q] [?\C-g] [?\C-h] [?\e ?\C-c] [?\C-u] [?\e ?x] [?\e ?:] [?\e ?!] [?\e ?&] [?\C-o]))
  (add-hook 'eshell-load-hook #'eat-eshell (add-hook 'eshell-load-hook #'eat-eshell-visual-command-mode))
  (defun af/override-eat-semi-char-bindings ()
    (define-key eat-semi-char-mode-map "\M-j" (af/unread-key-sequence "C-b"))
    (define-key eat-semi-char-mode-map "\M-l" (af/unread-key-sequence "C-f"))
    (define-key eat-semi-char-mode-map "\M-i" (af/unread-key-sequence "C-p"))
    (define-key eat-semi-char-mode-map "\M-k" (af/unread-key-sequence "C-n"))
    (define-key eat-semi-char-mode-map "\M-o" (af/unread-key-sequence "M-f"))
    (define-key eat-semi-char-mode-map "\M-u" (af/unread-key-sequence "M-b")))
  (advice-add 'eat-semi-char-mode :after 'af/override-eat-semi-char-bindings))

(use-package wrap-region
  :ensure t
  :config
  (wrap-region-global-mode t)
  (wrap-region-add-wrappers
   '(("$" "$")
     ("{-" "-}" "#")
     ("/" "/")
     ("/* " " */" "#" (java-mode javascript-mode css-mode))
     ("`" "`" nil (markdown-mode)))))

(use-package expand-region
  :bind ("M-=" . er/expand-region))

(use-package helpful
  :config
  (global-set-key (kbd "C-h f") #'helpful-callable)
  (global-set-key (kbd "C-h v") #'helpful-variable)
  (global-set-key (kbd "C-h k") #'helpful-key))

(use-package avy
  :config
  (setq avy-timeout-seconds 0.3))

(use-package vertico
  :ensure t
  :config
  (setq vertico-cycle t)
  (setq vertico-resize nil)
  (vertico-mode 1))

(use-package marginalia
  :ensure t
  :config
  (marginalia-mode 1))

(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless basic)))

(use-package consult
  :ensure t
  :init
  (setq xref-show-xrefs-function #'consult-xref
	xref-show-definitions-function #'consult-xref)
  :bind
  ("C-x b" . consult-buffer))

(use-package yaml-mode)
(use-package json-mode)

(use-package modus-themes
  :config
  (load-theme 'modus-vivendi :no-confirm))

(use-package corfu
  :custom
  (corfu-separator ?\s) ;; Orderless field separator
  :defer t
  :bind (:map corfu-map
	      ("SPC" . corfu-insert-separator))
  :init
  (global-corfu-mode))

(unless (display-graphic-p)
  (straight-use-package
   '(corfu-terminal
     :type git
     :repo "https://codeberg.org/akib/emacs-corfu-terminal.git"))
  (use-package corfu-terminal
    :after (corfu)
    :config
    (corfu-terminal-mode +1)))

(use-package svg-lib)

(use-package kind-icon
  :ensure t
  :after corfu
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package xclip
  :ensure t
  :if (locate-file "xclip" (exec-path))
  :init
  (xclip-mode 1))

(use-package clipetty
  :unless (display-graphic-p)
  :hook (after-init . global-clipetty-mode)
  :bind ("C-c w" . clipetty-kill-ring-save))

(use-package jinx
  :hook (emacs-startup . global-jinx-mode)
  :if (or (locate-file "enchant" (exec-path))
	  (locate-file "enchant-2" (exec-path)))
  :bind (("M-$" . jinx-correct))
  :config
  (setq-local jinx-languages "en_CA pt_BR fr_FR"))

(use-package magit
  :ensure t)

(use-package transpose-frame
  :ensure t)

;; tabs utilities
(defun af/tabs-pick-name (&optional frame)
  (completing-read
   "Switch to tab: "
   (af/tabs-get-all-names frame)
   nil nil ""))

(defun af/tabs-get-all-names (&optional frame)
  (mapcar
   (lambda (tab) (cdr (assq 'name tab)))
   (frame-parameter (or frame (selected-frame)) 'tabs)))

(defun af/switch-to-tab (&optional frame)
  (interactive)
  (tab-switch (af/tabs-pick-name frame)))

(defun af/filter-terminal-names (mode)
  (let ((terminals ()))
    (dolist (buffer (buffer-list) terminals)
      (with-current-buffer buffer
	(when (eq major-mode mode)
	  (push (buffer-name buffer) terminals))))))

(defun af/switch-to-eat-terminal (mode prompt)
  (interactive)
  (require 'consult)
  (let* ((terminals (af/filter-terminal-names mode))
	 (selected (consult--read terminals :prompt prompt :state (consult--buffer-preview) :require-match nil))
	 (buff (get-buffer selected)))
    (if buff
	(with-current-buffer buff
	  (if (eq major-mode mode)
	      (switch-to-buffer buff)
	    (message (concat "Existing buffer " selected " is not from mode " mode ". Aborting."))))
      (cond ((eq mode 'eat-mode) (let ((new-buff (eat)))
				   (switch-to-buffer new-buff)
				   (with-current-buffer new-buff
				     (rename-buffer (concat selected)))))
	    ((eq mode 'eshell-mode) (let ((new-buff (eshell)))
				      (switch-to-buffer new-buff)
				      (with-current-buffer new-buff
					(rename-buffer (concat selected)))))
	    (t (message "Unknown major mode" mode))))))

(use-package hydra
  :ensure t
  :after (key-chord expand-region))

(defun af/derived-eat-mode ()
  (derived-mode-p 'eat-mode))

(defhydra af/hydra-find (:color blue)
  ("0" delete-window "delete window")
  ("1" delete-other-windows "delete other windows")
  ("2" split-window-below "split below")
  ("3" split-window-right "split right")
  ("l" consult-line "line")
  ("L" (lambda () (interactive) (consult-line-multi "")) "line in buffers")
  ("i" consult-imenu "imenu")
  ("b" consult-buffer "buffer")
  ("t" (lambda () (interactive) (af/switch-to-eat-terminal 'eat-mode "Choose terminal: ")) "switch terminal")
  ;; ("T" af/switch-to-tab "switch tab")
  ("f" consult-find "find file")
  ("F" project-find-file "Find file in project")
  ("d" dired "dired")
  ("g" consult-grep "grep")
  ;; ("s" (lambda () (interactive) (af/switch-to-eat-terminal 'eshell-mode "Choose eshell: ")) "switch eshell")
  ("o" consult-outline "outline")
  ("m" consult-mark "mark")
  ("r" query-replace-regexp "replace regexp")
  ("M" consult-global-mark "global mark")
  ("RET" nil))

(use-package macrostep)

(use-package dumb-jump
  :ensure t
  :defer t
  :custom
  (xref-show-definitions-function #'consult-xref)
  :init
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

(use-package emacs
  :init
  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete)

  ;; Emacs 30 and newer: Disable Ispell completion function. As an alternative,
  ;; try `cape-dict'.
  (setq text-mode-ispell-word-completion nil)

  ;; Emacs 28 and newer: Hide commands in M-x which do not apply to the current
  ;; mode.  Corfu commands are hidden, since they are not used via M-x. This
  ;; setting is useful beyond Corfu.
  (setq read-extended-command-predicate #'command-completion-default-include-p)
  :config
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  (setq make-backup-files nil)
  (setq backup-inhibited nil)
  (setq create-lockfiles nil)
  (setq custom-unlispify-tag-names nil)
  ;; Make native compilation silent and prune its cache.
  (when (native-comp-available-p)
    (setq native-comp-async-report-warnings-errors 'silent) ; Emacs 28 with native compilation
    (setq native-compile-prune-cache t)) ; Emacs 29

  (setq custom-file (make-temp-file "emacs-custom-"))

  (keymap-global-set "M-j" 'backward-char)
  (keymap-global-set "M-k" 'next-line)
  (keymap-global-set "M-i" 'previous-line)
  (keymap-global-set "M-l" 'forward-char)
  (keymap-global-set "M-u" 'backward-word)
  (keymap-global-set "M-o" 'forward-word)
  (keymap-global-set "M-;" (lambda () (interactive) (af/switch-to-eat-terminal 'eat-mode "Choose terminal: ")))
  (keymap-global-set "M-s" 'af/hydra-find/body)
  (keymap-global-set "C-o" 'other-window)
  (keymap-global-set "M-h" 'consult-buffer)

  (global-set-key "\e[9~" 'set-mark-command)

  (put 'downcase-region 'disabled nil)
  (put 'upcase-region 'disabled nil)

  (setq shift-select-mode 'permanent)
  (setq initial-buffer-choice t)
  (auto-revert-mode 1)
  (savehist-mode 1)
  (recentf-mode 1)
  (delete-selection-mode))

(use-package golden-ratio
  :config
  (golden-ratio-mode 1)
  (setq golden-ratio-auto-scale t))

(use-package winner
  :config
  (winner-mode 1))

(use-package repeat
  :config
  (setq repeat-on-final-keystroke t)
  (setq set-mark-command-repeat-pop t)
  (setq repeat-exit-key "RET")

  (repeat-mode 1)

  (defvar isearch-repeat-map
    (let ((map (make-sparse-keymap)))
      (define-key map (kbd "s") #'isearch-repeat-forward)
      (define-key map (kbd "r") #'isearch-repeat-backward)
      map))

  (dolist (cmd '(isearch-repeat-forward isearch-repeat-backward))
    (put cmd 'repeat-map 'isearch-repeat-map)))

(use-package lsp-mode
  :custom
  (lsp-completion-provider :none)
  (setq lsp-keymap-prefix "C-c l")
  :init
  (defun af/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
	  '(orderless)))
  :hook
  (lsp-completion-mode . af/lsp-mode-setup-completion)
  :commands lsp)
